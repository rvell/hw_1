public class ColorSort {

   enum Color {red, green, blue};
   
   public static void main (String[] param) {
      // for debugging
   }
   
   public static void reorder (Color[] balls) {
      int rCount = 0;
      int gCount = 0;
      int bCount = 0;
      Color[] newBalls = new Color[ rCount + gCount + bCount];

      for ( int i = 0; i < balls.length; i++ ){
         if (balls[i] == ColorSort.Color.red){
            rCount++;
            System.out.println("The current number of red balls is: " + rCount);
         }
         else if (balls[i] == ColorSort.Color.green){
            gCount++;
            System.out.println("The current number of green balls is: " + gCount);
         }
         else if (balls[i] == ColorSort.Color.blue){
            bCount++;
            System.out.println("The current number of blue balls is: " + bCount);
         }
      }
      System.out.println("The story so far: " + rCount + " red balls, " + gCount + " green balls, " + bCount + " blue balls.");
   }
}

